"""
    UTE (Administración Nacional de Usinas y Trasmisiones Eléctricas) API Wrapper
    Copyright (C) 2023 Roger Gonzalez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from datetime import datetime, timedelta
from time import sleep
from typing import List, Optional

import requests

BASE_URL = "https://rocme.ute.com.uy/customersapp"
LOGIN_URL = "https://identityserver.ute.com.uy/connect/token"

class UTEClient:
    def __init__(
        self,
        document: str,
        password: str,
        device_id: str = None,
        average_cost_per_kwh: float = None,
        power_factor: float = None,
    ):
        self.document = document
        self.password = password
        self.device_id = device_id
        self.average_cost_per_kwh = average_cost_per_kwh
        self.authorization = self._login()
        self.power_factor = power_factor

        if self.power_factor and (self.power_factor < 0 or self.power_factor > 1):
            raise Exception("Your power factor has to be between 0 and 1")

        if not self.device_id:
            devices = self.get_devices_list()
            
            if len(devices) > 1:
                devices_dict = {}
                for device in devices:
                    devices_dict[device["name"]] = device["alias"]

                raise Exception(
                    f"""
                You have multiple device IDs. You need to choose one from the list
                Valid options are: {devices_dict}
                """
                )

            self.device_id = devices[0]["accountId"]
            self.device = self.get_account()[0]
            self.agreement_id = self.device['serviceAgreementId']
            self.service_point_id = self.device['servicePointId']

        if not self.average_cost_per_kwh:
            try:
                tariff_type = self.get_account()[0]["tariff"].lower()
                self.average_cost_per_kwh = self.get_average_price(tariff_type)
            except Exception:
                raise Exception("Your tariff type is not standard. Try making it explicit on the client initialization")

    def _make_request(self, method: str, url: str, data: Optional[dict] = None, headers: Optional[dict] = None) -> requests.Response:
        """
        Make a HTTP request

        Args:
            method (str): The HTTP method to use. Accepted methods are ``GET``, ``POST``.
            url (str): The URL to use for the request.
            authorization (str): Authorization token
            data (dict): The data to send in the body of the request.

        Returns:
            requests.Response: The response object.

        Raises:
            Exception: If the method is not supported.
        """
        if headers == None:
            headers = {
                "accept-encoding": "gzip",
                "host": "rocme.ute.com.uy",
                "user-agent": "Dart/3.0"
            }

        try:
            if self.authorization:
                headers["Authorization"] = f"{self.authorization['token_type']} {self.authorization['access_token']}"
        except AttributeError:
            pass

        if method == "GET":
            return requests.get(url, headers=headers)

        if method == "POST":
            if url == LOGIN_URL:
                return requests.post(url, headers=headers, data=data)
            return requests.post(url, headers=headers, json=data)
        
        raise Exception("Method not supported")

    def _login(self) -> str:
        """
        Login to UTE

        Args:
            email (str): User email for authentication
            phone_number (str): User phone number for authentication

        Returns:
            str: Authorization token
        """

        url = f"{LOGIN_URL}"
        headers = {
            "accept-encoding": "gzip",
            "authorization": "Basic ",
            "content-type": "application/x-www-form-urlencoded; charset=utf-8",
            "host": "identityserver.ute.com.uy",
        }
        data = {
            'grant_type': 'password',
            "username": self.document,
            "password": self.password,
        }

        return self._make_request("POST", url, data=data, headers=headers).json()

    def get_devices_list(self) -> List[dict]:
        """
        Get UTE devices list

        Returns:
            List[dict]: List of devices
        """

        accounts_url = f"{BASE_URL}/accounts"
        return self._make_request("GET", accounts_url).json()

    def get_account(self) -> dict:
        """
        Get UTE account info from device id

        Returns:
            dict: UTE account information
        """
        
        accounts_by_id_url = f"{BASE_URL}/accounts/{self.device_id}/services"
        return self._make_request("GET", accounts_by_id_url).json()

    def get_peak(self) -> dict:
        """
        Get UTE peak info from device id

        Returns:
            dict: UTE peak info
        """

        peak_by_id_url = f"{BASE_URL}/accounts/{self.device_id}/services/{self.agreement_id}/peak"
        return self._make_request("GET", peak_by_id_url).json()

    def get_network_status(self) -> List[dict]:
        """
        Get UTE network status from device id

        Returns:
            dict: UTE network status
        """

        network_status_url = f"{BASE_URL}/accounts/{self.device_id}/services/{self.agreement_id}/{self.service_point_id}/status"
        return self._make_request("GET", network_status_url).json()

    def get_renewable_sources(self) -> str:
        """
        Get UTE renewable sources

        Returns:
            str: UTE renewable sources percentage
        """

        global_demand_url = f"{BASE_URL}/accounts/{self.device_id}/services/{self.agreement_id}/quality"
        return self._make_request("GET", global_demand_url).json()["demand"]["renewableSources"]

    def get_historic_consumption(
        self,
        period: Optional[str] = "D",
        date_start: Optional[str] = None,
        date_end: Optional[str] = None,
    ) -> dict:
        """
        Generate UTE historic consumption from device id and date range

        Args:
            period (str): "H" if you want hourly data, "D" if you want daily data, defaults to "D"
            date_start (str): Start date to check in format YYYY-MM-DD
            date_end (str): End date to check in format YYYY-MM-DD

        Returns:
            dict: UTE info
        """

        if date_start is None:
            yesterday = datetime.now() - timedelta(days=1)
            date_start = yesterday.strftime("%Y-%m-%d")

        if date_end is None:
            yesterday = datetime.now() - timedelta(days=1)
            date_end = yesterday.strftime("%Y-%m-%d")

        historic_url = (
            f"{BASE_URL}/accounts/miConsumptionCurve/{self.service_point_id}/{period}/{date_start}/{date_end}"
        )

        response = self._make_request("GET", historic_url).json()

        active_energy = {"total": {"sum_in_kwh": 0}}
        try:
            for item in response:
                if item["magnitudeVO"] == "IMPORT_ACTIVE_ENERGY":
                    date = datetime.strptime(item["date"], "%Y-%m-%dT%H:%M:%S%z")
                    day_in_week = date.strftime("%A")
                    value = round(float(item["value"]), 3)

                    active_energy[date.strftime("%d/%m/%Y")] = {
                        "kwh": value,
                        "aproximated_cost_in_uyu": round(value * self.average_cost_per_kwh, 3),
                        "day_in_week": day_in_week,
                    }
                    active_energy["total"]["sum_in_kwh"] += value

            active_energy["total"]["aproximated_cost_in_uyu"] = round(
                active_energy["total"]["sum_in_kwh"] * self.average_cost_per_kwh, 3
            )
            active_energy["total"]["daily_average_cost"] = round(
                active_energy["total"]["aproximated_cost_in_uyu"] / (len(active_energy) - 1), 3
            )
            return active_energy
        except ZeroDivisionError:
            return "No data available for this period of time"

    def _convert_powers_to_power_in_watts(self, readings: List[dict]) -> float:
        """
        Convert powers to power in watts and determine the system type (monophasic, biphasic, or triphasic)
        automatically.

        Args:
            readings (List[dict]): List of readings

        Returns:
            float: Power in watts
        """
        reading_sums = {"I1": 0, "I2": 0, "I3": 0, "V1": 0, "V2": 0, "V3": 0}
        num_voltages = num_currents = 0
        total_power_in_watts = 0
        square_root_of_three = 1.732

        for reading in readings:
            reading_type = reading["tipoLecturaMGMI"]
            if reading_type in reading_sums:
                reading_sums[reading_type] += float(reading["valor"])
                if "V" in reading_type:
                    num_voltages += 1
                elif "I" in reading_type:
                    num_currents += 1

        if num_voltages > 0 and num_currents > 0:
            averaged_voltage = sum(reading_sums[v] for v in ["V1", "V2", "V3"]) / num_voltages
            averaged_current = sum(reading_sums[i] for i in ["I1", "I2", "I3"]) / num_currents

            if num_voltages == 3 and num_currents == 3:
                total_power_in_watts = averaged_voltage * averaged_current * self.power_factor * square_root_of_three
            elif num_voltages == 2 and num_currents == 2:
                total_power_in_watts = averaged_voltage * averaged_current * self.power_factor * square_root_of_three
            else:
                total_power_in_watts = averaged_voltage * averaged_current * self.power_factor

        return round(total_power_in_watts, 3)

    def get_current_usage_info(self) -> dict:
        """
        Get current usage info from device id

        Args:
            device_id (str): UTE Device id
            authorization (str): Authorization token

        Returns:
            dict: UTE info

        Raises:
            Exception: If the reading request fails
        """

        reading_request_url = f"{BASE_URL}/accounts/meters/read"
        reading_url = f"{BASE_URL}/accounts/{self.device_id}/services/{self.agreement_id}/{self.service_point_id}/meters/last"

        data = {
            "accountId": self.device_id,
            "agreementId": self.agreement_id,
            "servicePointId": self.service_point_id,
            }

        reading_request = self._make_request("POST", reading_request_url, data=data)

        if reading_request.status_code != 200:
            raise Exception("Error getting reading request")

        response = self._make_request("GET", reading_url)

        while response.status_code == 204:
            sleep(5)
            response = self._make_request("GET", reading_url)
            #print(response)

        readings = response.json()["data"]["readings"] #This will probably change based on the other endpoints, but i haven`t get any response from the request so it's hard to know

        power_in_watts = self._convert_powers_to_power_in_watts(readings)

        return_dict = {**response}
        return_dict["data"]["power_in_watts"] = power_in_watts #This will probably change based on the other endpoints, but i haven`t get any response from the request so it's hard to know
        return_dict["data"]["using_power_factor"] = True if self.power_factor else False #This will probably change based on the other endpoints, but i haven`t get any response from the request so it's hard to know

        return return_dict

    def get_average_price(self, plan: str) -> float:
        """
        Get the average price for a plan

        Args:
            plan (str): Plan name. Can be "triple" or "doble"

        Returns:
            float: Average price

        Raises:
            Exception: If the plan is invalid
        """

        if plan == "trt":
            # 11.032 UYU/kwh * 16.67% of the day (4 hours)
            # 2.298 UYU/kwh * 29.17% of the day (7 hours)
            # 5.036 UYU/kwh * 54.16% of the day (13 hours)
            return (11.032 * 0.1667) + (2.298 * 0.2917) + (5.036 * 0.5416)
        if plan == "trd":
            # 11.032 UYU/kwh * 16.67% of the day (4 hours)
            # 4.422 UYU/kwh * 83.33% of the day (20 hours)
            return (1.032 * 0.1667) + (4.422 * 0.8333)

        raise Exception("Invalid plan")
