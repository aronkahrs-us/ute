# UTE API Wrapper 🇺🇾

This Python package provides a convenient wrapper for interacting with the [UTE (Administración Nacional de Usinas y Trasmisiones Eléctricas)](https://portal.ute.com.uy/) API in Uruguay 🇺🇾. It allows you to retrieve various information related to your UTE account, electricity consumption, network status, and more.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Examples](#examples)
- [Contributing](#contributing)
- [License](#license)

## Installation

You can install the UTE API Wrapper using pip:

```bash
pip install ute-wrapper
```

## Usage

Import the `UTEClient` class from the package and create an instance with your UTE account details:

```python
from ute_wrapper.ute import UTEClient

document = "12345678"#(1.234.567-8)
password = "example123"
device_id = "your_device_id"  # Optional
average_cost_per_kwh = 4.0  # Optional, your average cost per kWh in UYU
power_factor = 0.7 # Optional, your power factor

ute_client = UTEClient(document, password, device_id, average_cost_per_kwh, power_factor)
```

### Available Methods

- `get_devices_list()`: Get a list of UTE devices associated with the account.
- `get_account()`: Get UTE account information for the specified device ID.
- `get_peak()`: Get UTE peak information for the specified device ID.
- `get_network_status()`: Get UTE network status information.
- `get_renewable_sources()`: Get the percentage of UTE renewable sources.
- `get_historic_consumption(period="D",date_start=None, date_end=None)`: Get historic UTE consumption information within a specified date range.
- `get_current_usage_info()`: Get current usage information for the specified device ID.
- `get_average_price(plan)`: Get the average price for a specific UTE plan ("triple" or "doble").

## Examples

### Get Historic Consumption

```python
historic_consumption = ute_client.get_historic_consumption(period="H",date_start="2023-08-01", date_end="2023-08-15")
print(historic_consumption)
```

### Get Current Usage Info

```python
current_usage_info = ute_client.get_current_usage_info()
print(current_usage_info)
```
## Changelog
## [1.3.0] - 2024-01-09
  
Changed to the new "API" from the new UTE App (UTE Clientes), the ```get_current_usage_info``` doesn't return anything(at least for me, not even the app shows anything), but should work eventually
 
### Added
- [LOGIN_URL](https://gitlab.com/rogs/ute/-/blob/master/src/ute_wrapper/ute.py?ref_type=heads#L26)
for authentication
### Changed
- [Login parameters](https://gitlab.com/rogs/ute/-/blob/master/src/ute_wrapper/ute.py?ref_type=heads#L31) Changed ```email``` and ```phone_number``` to ```document``` and ```password```
- [_login](https://gitlab.com/rogs/ute/-/blob/master/src/ute_wrapper/ute.py?ref_type=heads#L113)
The authentication method changed with the new "API", it's done via the ```identityserver```
- [_make_request](https://gitlab.com/rogs/ute/-/blob/master/src/ute_wrapper/ute.py?ref_type=heads#L74)
To consider login requests(post request sends data instead of json)
- [get_historic_consumption](https://gitlab.com/rogs/ute#get-historic-consumption)
  Added "period" argument
- Changed some of the endpoints to work with the new "API"
## Contributing

Contributions are welcome! If you find a bug or have a suggestion, please create an issue or submit Merge Request on [Gitlab](https://gitlab.com/rogs/ute).

## License

This project is licensed under the GNU General Public License, version 3.0. For more details, see [LICENSE](LICENSE).

---

*This project is not affiliated with UTE (Administración Nacional de Usinas y Trasmisiones Eléctricas) or its affiliates.*
